﻿// SPDX-License-Identifier: GPL-3.0-or-later
/*
 * QuiCdKeyGen: NEOGEO Arcade Stick Pro Unlock Key Generator
 * Copyright (C) 2020  Yukai Li
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Security.Cryptography;
using CSInteropKeys;

namespace QuiCdkeyGen
{
    static class PemParser
    {
        public static RSAParameters ParseRsaPrivateKeyPem(StreamReader inputReader)
        {
            if (inputReader == null) throw new ArgumentNullException(nameof(inputReader));

            if (inputReader.ReadLine() != "-----BEGIN RSA PRIVATE KEY-----")
                throw new InvalidDataException("File does not have expected starting line");

            bool foundEnd = false;
            StringBuilder sb = new StringBuilder();
            while (!inputReader.EndOfStream && !foundEnd)
            {
                var line = inputReader.ReadLine();
                if (line == "-----END RSA PRIVATE KEY-----")
                    foundEnd = true;
                else
                    sb.Append(line);                    
            }

            if (!foundEnd)
                throw new InvalidDataException("Reached end of file without finding ending line");

            byte[] asn1Data = Convert.FromBase64String(sb.ToString());
            return new AsnKeyParser(asn1Data).ParseRsaPublicKeyPlain();
        }
    }
}
