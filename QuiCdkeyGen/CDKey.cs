﻿// SPDX-License-Identifier: GPL-3.0-or-later
/*
 * QuiCdKeyGen: NEOGEO Arcade Stick Pro Unlock Key Generator
 * Copyright (C) 2020  Yukai Li
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
using System;
using System.Collections.Generic;
using System.Text;

namespace QuiCdkeyGen
{
    public class CDKey
    {
        string name;

        public int Id { get; set; }
        public string Name
        {
            get => name;
            set
            {
                if (value == null) throw new ArgumentNullException(nameof(value));
                name = value;
            }
        }

        public CDKey(int id, string name)
        {
            Id = id;
            Name = name;
        }
    }
}
