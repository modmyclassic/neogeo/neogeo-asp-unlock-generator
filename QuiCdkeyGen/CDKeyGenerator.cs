﻿// SPDX-License-Identifier: GPL-3.0-or-later
/*
 * QuiCdKeyGen: NEOGEO Arcade Stick Pro Unlock Key Generator
 * Copyright (C) 2020  Yukai Li
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
using System;
using System.Collections.Generic;
using System.Text;
using System.Security.Cryptography;
using System.IO;

namespace QuiCdkeyGen
{
    public static class CDKeyGenerator
    {
        public static string GenerateCDKeySingle(string serialNumber, CDKey cdKey)
        {
            if (serialNumber == null) throw new ArgumentNullException(nameof(serialNumber));
            if (cdKey == null) throw new ArgumentNullException(nameof(cdKey));

            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("[SN_NO]=<{0}>\n", serialNumber);
            sb.Append("[CDKEY]\n");
            sb.AppendFormat("[ID]=<{0}>\n", cdKey.Id);
            sb.AppendFormat("[NAME]=<{0}>\n", cdKey.Name);
            sb.Append("[CDKEY/]\n");
            string toEncrypt = string.Format("{0}{1}{2}{3}",
                serialNumber,
                cdKey.Id,
                cdKey.Name,
                1
            );
            sb.AppendFormat("[ENCRYPT]=<{0}>\n", GenerateMD5Hash(toEncrypt));
            return sb.ToString();
        }

        public static void GenerateCDKeyArchive(Stream outputStream, string serialNumber, IList<CDKey> cdKeys, RSAParameters rsaKey)
        {
            if (outputStream == null) throw new ArgumentNullException(nameof(outputStream));
            if (!outputStream.CanSeek) throw new ArgumentException("Stream is not seekable.", nameof(outputStream));
            if (serialNumber == null) throw new ArgumentNullException(nameof(serialNumber));
            if (cdKeys == null) throw new ArgumentNullException(nameof(cdKeys));

            BinaryWriter bw = new BinaryWriter(outputStream);
            int[] encryptedLengths = new int[cdKeys.Count];

            // Write dummy header
            bw.Write(0); // File length excluding this field
            bw.Write(cdKeys.Count); // Number of keys in archive
            for (int i = 0; i < encryptedLengths.Length; ++i)
                bw.Write(0); // Length of each encrypted key
            bw.Write(new byte[32]); // Header hash

            using (RSA rsa = RSA.Create(rsaKey))
            {
                // Encrypt and write key files
                for (int i = 0; i < cdKeys.Count; ++i)
                {
                    string cdKeyFile = GenerateCDKeySingle(serialNumber, cdKeys[i]);
                    byte[] dataToCrypt = Encoding.ASCII.GetBytes(cdKeyFile);
                    byte[] encryptedData = rsa.Encrypt(dataToCrypt, RSAEncryptionPadding.Pkcs1);
                    encryptedLengths[i] = encryptedData.Length;
                    bw.Write(encryptedData);
                }
            }

            // Write real header values
            outputStream.Seek(0, SeekOrigin.Begin);
            bw.Write((int)outputStream.Length - 4); // File length excluding this field
            bw.Seek(4, SeekOrigin.Current); // Skip number of keys
            for (int i = 0; i < encryptedLengths.Length; ++i)
                bw.Write(encryptedLengths[i]); // Length of each encrypted key
            // Generate and write header hash
            bw.Write(GenerateArchiveHeaderHash(encryptedLengths).ToCharArray());
        }

        public static IList<byte[]> DecryptCDKeyArchive(Stream inputStream, RSAParameters rsaKey)
        {
            if (inputStream == null) throw new ArgumentNullException(nameof(inputStream));

            BinaryReader br = new BinaryReader(inputStream);
            if (br.ReadInt32() != inputStream.Length - 4)
                throw new InvalidDataException("Archive length mismatch");
            int numKeys = br.ReadInt32();
            int[] encryptedLengths = new int[numKeys];
            for (int i = 0; i < encryptedLengths.Length; ++i)
                encryptedLengths[i] = br.ReadInt32();

            string calculatedHeaderHash = GenerateArchiveHeaderHash(encryptedLengths);
            string headerHash = new string(br.ReadChars(32));
            if (headerHash != calculatedHeaderHash)
                throw new InvalidDataException("Header hash mismatch");

            // Now on to the fun stuff
            List<byte[]> files = new List<byte[]>();
            using (RSA rsa = RSA.Create(rsaKey))
            {
                foreach (var length in encryptedLengths)
                {
                    byte[] encryptedData = br.ReadBytes(length);
                    byte[] decryptedData = rsa.Decrypt(encryptedData, RSAEncryptionPadding.Pkcs1);
                    files.Add(decryptedData);
                }
            }

            return files;
        }

        static string GenerateArchiveHeaderHash(int[] lengths)
        {
            StringBuilder hashSb = new StringBuilder();
            hashSb.Append(lengths.Length);
            foreach (int length in lengths)
                hashSb.Append(length);
            return GenerateMD5Hash(hashSb.ToString());
        }

        static string GenerateMD5Hash(string s)
        {
            byte[] toHash = Encoding.ASCII.GetBytes(s);
            byte[] computedHash;
            using (MD5 md5 = MD5.Create())
            {
                computedHash = md5.ComputeHash(toHash);
            }
            StringBuilder sb = new StringBuilder();
            foreach (byte b in computedHash)
            {
                sb.AppendFormat("{0:x2}", b);
            }
            return sb.ToString();
        }
    }
}
