﻿// SPDX-License-Identifier: GPL-3.0-or-later
/*
 * QuiCdKeyGen: NEOGEO Arcade Stick Pro Unlock Key Generator
 * Copyright (C) 2020  Yukai Li
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
using System;
using System.IO;
using System.Security.Cryptography;
using System.Collections.Generic;

namespace QuiCdkeyGen
{
    class Program
    {
        static void PrintUsage(string progName)
        {
            Console.Error.WriteLine("Usage:");
            Console.Error.WriteLine("{0} [-s serialNumber] <-d inputPath | outputPath> pemPath [gameId gameName] ...", progName);
            Console.Error.WriteLine("\t-s serialNumber: Provide the serial number to lock key file to. Omit to make key file usable on all units.");
            Console.Error.WriteLine("\t-d inputPath: Decrypt key files from the archive specified with inputPath.");
            Console.Error.WriteLine("\toutputPath: Generate key file archive and write to outputPath.");
            Console.Error.WriteLine("\tpemPath: The path to the PEM formatted RSA private key.");
            Console.Error.WriteLine("\tgameId gameName: Game ID and name pairs. Multiple pairs can be specified. Cannot be specified if decrypting.");
        }

        static void Main(string[] args)
        {
            Console.Error.WriteLine("NEOGEO Arcade Stick Pro Unlock Key Generator");
            Console.Error.WriteLine("(C) 2020 cyanic");
            Console.Error.WriteLine();

            var argv = Environment.GetCommandLineArgs();
            if (argv.Length == 1)
            {
                PrintUsage(argv[0]);
                return;
            }

            string serialNumber = null;
            bool isBuild = true;
            string filePath = null;
            string pemPath = null;
            List<CDKey> keys = new List<CDKey>();
            int gameIdArg = 0;

            // My stupid arguments parser
            int parseState = 0;
            foreach (var arg in args)
            {
                switch (parseState)
                {
                    case 0: // Not in option
                        switch (arg.ToLower())
                        {
                            case "-s":
                                if (serialNumber != null)
                                {
                                    Console.Error.WriteLine("Serial number already specified.");
                                    Console.Error.WriteLine();
                                    PrintUsage(argv[0]);
                                    return;
                                }
                                parseState = 1;
                                break;
                            case "-d":
                                if (!isBuild)
                                {
                                    Console.Error.WriteLine("Decrypt option already specified.");
                                    Console.Error.WriteLine();
                                    PrintUsage(argv[0]);
                                    return;
                                }
                                isBuild = false;
                                parseState = 2;
                                break;
                            default:
                                if (filePath == null)
                                {
                                    filePath = arg;
                                    break;
                                }

                                if (pemPath == null)
                                {
                                    pemPath = arg;
                                    break;
                                }

                                if (!isBuild)
                                {
                                    Console.Error.WriteLine("Too many arguments.");
                                    Console.Error.WriteLine();
                                    PrintUsage(argv[0]);
                                    return;
                                }

                                if (!int.TryParse(arg, out gameIdArg))
                                {
                                    Console.Error.WriteLine("Game ID is not a number.");
                                    Console.Error.WriteLine();
                                    PrintUsage(argv[0]);
                                    return;
                                }
                                parseState = 3;
                                break;
                        }
                        break;
                    case 1: // Get serial number
                        serialNumber = arg.Replace("-", string.Empty).ToUpper();
                        if (serialNumber.Length != 14)
                        {
                            Console.Error.WriteLine("Invalid serial number.");
                            Console.Error.WriteLine();
                            PrintUsage(argv[0]);
                            return;
                        }
                        parseState = 0;
                        break;
                    case 2: // Get decrypt input path
                        filePath = arg;
                        parseState = 0;
                        break;
                    case 3: // Get game name and create key
                        keys.Add(new CDKey(gameIdArg, arg));
                        parseState = 0;
                        break;
                }
            }

            if (parseState != 0)
            {
                Console.Error.WriteLine("Argument missing.");
                Console.Error.WriteLine();
                PrintUsage(argv[0]);
                return;
            }

            if (filePath == null)
            {
                Console.Error.WriteLine("Unlock key file path not specified.");
                Console.Error.WriteLine();
                PrintUsage(argv[0]);
                return;
            }

            if (pemPath == null)
            {
                Console.Error.WriteLine("RSA key file path not specified.");
                Console.Error.WriteLine();
                PrintUsage(argv[0]);
                return;
            }

            if (isBuild && serialNumber == null)
            {
                Console.Error.WriteLine("This version cannot generate keys not bound to a specific unit.");
                Console.Error.WriteLine();
                PrintUsage(argv[0]);
                return;
            }

            // Do work
            try
            {
                RSAParameters rsaKey;
                using (StreamReader sr = File.OpenText(pemPath))
                {
                    rsaKey = PemParser.ParseRsaPrivateKeyPem(sr);
                }

                if (isBuild)
                {
                    using (FileStream fs = File.Create(filePath))
                    {
                        CDKeyGenerator.GenerateCDKeyArchive(fs, serialNumber, keys, rsaKey);
                    }
                }
                else
                {
                    using (FileStream fs = File.OpenRead(filePath))
                    {
                        var decryptedKeys = CDKeyGenerator.DecryptCDKeyArchive(fs, rsaKey);
                        if (decryptedKeys.Count == 1)
                        {
                            File.WriteAllBytes(Path.ChangeExtension(filePath, ".ini"), decryptedKeys[0]);
                        }
                        else
                        {
                            for (int i = 0; i < decryptedKeys.Count; ++i)
                            {
                                File.WriteAllBytes(
                                    Path.Combine(Path.GetDirectoryName(filePath),
                                        string.Format("{0}_{1}.ini", Path.GetFileNameWithoutExtension(filePath), i)),
                                    decryptedKeys[i]
                                );
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine("Error while processing: {0}", ex.Message);
                Environment.Exit(1);
            }
        }
    }
}
