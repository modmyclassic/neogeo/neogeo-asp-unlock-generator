NEOGEO Arcade Stick Pro Unlock Key Generator
============================================

This program generates unlock keys for use with the NEOGEO Arcade Stick Pro
given an RSA private key and a list of games to unlock

Usage
-----
```
QuiCdkeyGen [-s serialNumber] <-d inputPath | outputPath> pemPath [gameId gameName] ...
```

* `-s serialNumber`: Provide the serial number to lock key file to. Omit to make
  key file usable on all units.
* `-d inputPath`: Decrypt key files from the archive specified with inputPath.
* `outputPath`: Generate key file archive and write to outputPath.
* `pemPath`: The path to the PEM formatted RSA private key.
* `gameId gameName`: Game ID and name pairs. Multiple pairs can be specified.
  Cannot be specified if decrypting.

Example
-------
```
QuiCdkeyGen -s 123456789ABCDE cdkey.cdk path\to\PRIVATE_KEY.key 1 example
```
This will write a key to `cdkey.cdk` tied to the device with the serial number
`123456789ABCDE` unlocking game with ID `1` and directory name `example`,
encrypted using the key at `path\to\PRIVATE_KEY.key`.

```
QuiCdkeyGen -d cdkey.cdk path\to\PRIVATE_KEY.key
```
This will extract the contents of `cdkey.cdk` to the directory it is in, using
the key at `path\to\PRIVATE_KEY.key` to decrypt. Output filename is based on
the input file's name. For example, if there is only one unlock inside the file,
the decrypted unlock file will be stored in `cdkey.ini`. If there are more
than one, they will be stored under `cdkey_x.ini`, where `x` is the index of
the unlock file.

Note
----
To keep things fair for SNK, this version of the program can only generate
unlock keys tied to a specific unit. It will not generate unlock keys that can
be used on any unit.
